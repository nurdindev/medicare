<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'medicare_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '})|b-:%mpF^#iPl8OMVT#-BNX3bidrvW/s?KsnSSy_rQ5O3>#_dd9:.bU(*d6]OA' );
define( 'SECURE_AUTH_KEY',   '68)p;TOb;B>akRM9i2h7ow67fNN@GM?K|3*>qiK>Jg3{7i7F9S4SI` JB``WN-?B' );
define( 'LOGGED_IN_KEY',     'MuEX}EZvjMIlBVi:|)/t}3}dXvqkBy.jnq:[daWD!*q5%qJBR8q)xVN~kJOJf=9t' );
define( 'NONCE_KEY',         '[!bQhqB53@Bpc$Axi)cfXmfN0nd0f@<K|8 ,;x[%LqnmMd-d:.Nys3$x6jIFbUU#' );
define( 'AUTH_SALT',         '&x#w=zCgcGw`_/Yovyk5`wJn_R%o^R/T*G NZMrq}e.Ama585G6[<g)3w*ENP.q$' );
define( 'SECURE_AUTH_SALT',  '59&XLIx5k]#%Wj7Q:+nL[P>Ml{hx:bI *)3-~y<[=X}rXw>aOUxp4=O0[L,CJ0lT' );
define( 'LOGGED_IN_SALT',    '%@F1ecQ{uZr5/%Q3ER6|2Uv&?k{8xbphf*e?H;Z3!k~X@2$?w>[m*M7)J%i{?|f?' );
define( 'NONCE_SALT',        'f~wb]+.VyA0=vU,zSdlm#gQ-oo|e`(|h4$h_RQW=2BNSn7:5ueckbnHg3F-3^Zdd' );
define( 'WP_CACHE_KEY_SALT', '@lQK~|kbZ>_T63a~ss?RwYy/U? joQYZ$P4@~yQLH_{!y*5k#]]Zxo0lvBJk>)&}' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
